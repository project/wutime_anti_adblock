# wutime_anti_adblock
Drupal 8 Anti-Adblock - AdBlock Detection

A Drupal 8.x module to render an anti-AdBlock overlay (or notice) to the 
current visitor.

The module has the following features:

1. Themed options for overlay theme
2. Fully responsive
3. Option to allow usergroups to bypass detection
4. Option to disable closing the overlay
5. Delay timer on overlay
6. Option to show a notice instead of overlay
7. Testing mode to ignore permissions and show overlay to everyone
8. Overlay automatically bypassed for known search engine bots
9. Randomized overlay ID per page visit to prevent blocking

## Installation

1. Install the module
2. Configure options: /admin/config/development/wutime_anti_adblock
3. Setup permissions: /admin/people/permissions

## License
[GNU] GNU GENERAL PUBLIC LICENSE
